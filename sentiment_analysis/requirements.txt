tensorflow>=1.12.0
tensorflow-serving-api>=1.10.1
requests
bert
bert-tensorflow
pandas
tensorflow_hub
spacy==2.0.16
