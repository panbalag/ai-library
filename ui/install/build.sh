#!/usr/bin/env bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

IMAGE_REPOSITORY=${IMAGE_REPOSITORY:-quay.io/opendatahub/ai-library-ui:latest}

echo "Building ${IMAGE_REPOSITORY} from local"

cd ${DIR}/..
rm -rf build
yarn install
yarn build
s2i build ./build centos/nginx-114-centos7 ${IMAGE_REPOSITORY}
